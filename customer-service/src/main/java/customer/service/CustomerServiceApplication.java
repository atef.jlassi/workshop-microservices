package customer.service;

import customer.service.entities.Customer;
import customer.service.repository.CustomerRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;

@SpringBootApplication
public class CustomerServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(CustomerServiceApplication.class, args);
    }

    @Bean
    CommandLineRunner start(CustomerRepository customerRepository, RepositoryRestConfiguration repositoryRestConfiguration) {
        return args -> {
          repositoryRestConfiguration.exposeIdsFor(Customer.class);
          customerRepository.save(new Customer(null, "IBM", "IBM@gmail.com"));
          customerRepository.save(new Customer(null, "HP", "HP@gmail.com"));
          customerRepository.save(new Customer(null, "TELNET", "TELNET@gmail.com"));

          customerRepository.findAll().forEach(customer -> System.out.println(customer.toString()));
        };
    }
}
