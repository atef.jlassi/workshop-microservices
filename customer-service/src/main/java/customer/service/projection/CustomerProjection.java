package customer.service.projection;

import customer.service.entities.Customer;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "p1", types = Customer.class)
interface CustomerProjection {
    public Long getId();
    public String getName();
}
