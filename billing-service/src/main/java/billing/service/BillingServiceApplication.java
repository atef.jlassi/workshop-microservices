package billing.service;

import billing.service.entities.*;
import billing.service.repositories.BillRepository;
import billing.service.repositories.ProductItemRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.hateoas.PagedModel;

import java.util.Date;

@SpringBootApplication
@EnableFeignClients
public class BillingServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(BillingServiceApplication.class, args);
    }

    @Bean
    CommandLineRunner start(BillRepository billRepository, ProductItemRepository productItemRepository, CustomerService customerService, InventoryService inventoryService) {
        return args -> {
            Customer c1 = customerService.findCustomerById(1L);
            System.out.println("*******************");
            System.out.println("ID = "+ c1.getId());
            System.out.println("NAME = " + c1.getName());
            System.out.println("EMAIL = " + c1.getEmail());
            System.out.println("*******************");
            Bill bill = billRepository.save(new Bill(null, new Date(), c1.getId(), null, null));

            PagedModel<Product> products = inventoryService.findAllProducts();
            System.out.println("products: "+ products.getContent());
            products.getContent().forEach(product -> {
                productItemRepository.save(new ProductItem(null, product.getId(),null,  product.getPrice(), 30, bill));
            });
//            Product p1 = inventoryService.findProductById(1L);
//            System.out.println("*******************");
//            System.out.println("Product id: "+ p1.getId());
//            System.out.println("Product Name: "+ p1.getName());
//            System.out.println("Price: "+ p1.getPrice());
//            System.out.println("*******************");
//            productItemRepository.save(new ProductItem(null, p1.getId(), p1.getPrice(), 30, bill));
//            Product p2 = inventoryService.findProductById(2L);
//            productItemRepository.save(new ProductItem(null, p2.getId(), p2.getPrice(), 30, bill));
//            Product p3 = inventoryService.findProductById(3L);
//            productItemRepository.save(new ProductItem(null, p3.getId(), p3.getPrice(), 30, bill));
        };
    }
}
