package billing.service.controller;

import billing.service.entities.Bill;
import billing.service.entities.CustomerService;
import billing.service.entities.InventoryService;
import billing.service.repositories.BillRepository;
import billing.service.repositories.ProductItemRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BillRestController {

    private BillRepository billRepository;
    private ProductItemRepository productItemRepository;
    private CustomerService customerService;
    private InventoryService inventoryService;

    public BillRestController(BillRepository billRepository, ProductItemRepository productItemRepository, CustomerService customerService, InventoryService inventoryService) {
        this.billRepository = billRepository;
        this.productItemRepository = productItemRepository;
        this.customerService = customerService;
        this.inventoryService = inventoryService;
    }

    @GetMapping("/fullBill/{id}")
    public Bill getBill(@PathVariable(name = "id") Long id) {
        Bill bill=billRepository.findById(id).get();
        bill.setCustomer(customerService.findCustomerById(bill.getCustomerId()));
        bill.getProductItems().forEach(productItem -> {
            productItem.setProduct(inventoryService.findProductById(productItem.getProductId()));
        });
        return bill;
    }
}
