package inventory.service;

import inventory.service.entities.Product;
import inventory.service.repositories.ProductRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;

@SpringBootApplication
public class InventoryServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(InventoryServiceApplication.class, args);
    }

    @Bean
    CommandLineRunner start(ProductRepository productRepository, RepositoryRestConfiguration repositoryRestConfiguration) {
        return args -> {
          repositoryRestConfiguration.exposeIdsFor(Product.class);
          productRepository.save(new Product(null, "HP SCREEN", 2000));
          productRepository.save(new Product(null, "Imprimante DELL ", 6581));
          productRepository.save(new Product(null, "IBM PROC", 2314));
        };
    }
}
